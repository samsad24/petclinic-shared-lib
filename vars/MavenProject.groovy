def call() {
pipeline {
    
    agent any
    
    environment {
        
        TEAMS_WEBHOOK_URL = credentials('teams-webhook-url')
        
    }
    stages {
         stage('verify') {
            steps {
                sh './mvnw -v'
            }
        }
         stage('compile') {
            steps {
                sh './mvnw clean compile'
            }
        }
         stage('test') {
            steps {
                sh './mvnw test'
                junit 'target/surefire-reports/*.xml'
            }
        }
        stage('quality') { 
            steps {
                withSonarQubeEnv('SonarQube') {
                    sh './mvnw sonar:sonar -Pcoverage'
                    }
                }
        }
    }
    
    post {
        
        success {
            office365ConnectorSend color: '#03fc80', message: '[Sami] Build Ok', status: 'OK', webhookUrl: "$TEAMS_WEBHOOK_URL"
        }
        
        failure {
            office365ConnectorSend color: '#fc0356', message: '[Sami] Build Ko', status: 'KO', webhookUrl: "$TEAMS_WEBHOOK_URL"
        }
        
    }
}
}
